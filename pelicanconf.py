#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
from datetime import datetime

AUTHOR = 'JuanPi Carbajal'
SITENAME = 'WABEsense blog'
SITEURL = ''
SITELOGO = SITEURL + '/images/wabesense_logo.svg'
SITETITLE = "WABEsense"
SITESUBTITLE = "Digitalization of alpine water springboxes"
SITEDESCRIPTION = "WABEsense - Digitalization of alpine water springboxes."

ROBOTS = "index, follow"


PATH = 'content'
OUTPUT_PATH = 'public'
STATIC_PATHS = ['images']

TIMEZONE = 'Europe/Zurich'

THEME = 'theme/flex'

# Content licensing: CC-BY-SA
CC_LICENSE = {
    "name": "Creative Commons Attribution-ShareAlike",
    "version": "4.0",
    "slug": "by-sa"
}
COPYRIGHT_YEAR = datetime.now().year
COPYRIGHT_NAME = "WABEsense Team"

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

USE_FOLDER_AS_CATEGORY = False
MAIN_MENU = True
HOME_HIDE_TAGS = True

# Blogroll
LINKS = (
    ('project repositories', 'https://gitlab.ost.ch/sciceg/lippunerag/wabesense'),
)

MENUITEMS = (('Archives', '/archives.html'),
             ('Categories', '/categories.html'),
             ('Tags', '/tags.html'),
             ('Authors', '/authors.html'))

# Social widget
SOCIAL = (
    ('twitter', 'https://twitter.com/JuanPiCarbajal'),
    ('gitlab', 'https://gitlab.com/hsr-iet/wabesense/blog'),
    ('rss', '/feeds/all.rss.xml'),
    ('atom', '/feeds/all.atom.xml'),
)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

# Plugins
# Path to Plugins
PLUGIN_PATHS = ['plugins']

PLUGINS = [
  "render_math",
  "pelican-cite",
  "i18n_subsites"
  ]

# Enable Jinja2 i18n extension used to parse translations.
JINJA_ENVIRONMENT = {"extensions": ["jinja2.ext.i18n"]}

# Translate to German.
DEFAULT_LANG = "de"
OG_LOCALE = "de_DE"
LOCALE = "de_DE"

#DEFAULT_LANG = "en"
#OG_LOCALE = "en_US"
#LOCALE = "en_US"

# Default theme language.
I18N_TEMPLATES_LANG = "en"
I18N_SUBSITES = {
    'en': {
        }
    }

PUBLICATIONS_SRC = 'content/references.bib'

THEME_COLOR_AUTO_DETECT_BROWSER_PREFERENCE = True
THEME_COLOR_ENABLE_USER_OVERRIDE = True

USE_LESS = True

DEFAULT_METADATA = {
    'status': 'draft',
}

MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.codehilite': {'css_class': 'highlight'},
        'markdown.extensions.extra': {},
        'markdown.extensions.meta': {},
        'markdown.extensions.toc': {},
    },
    'output_format': 'html5',
}

