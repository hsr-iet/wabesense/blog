Title: About
Date: 2020-08-05 16:00:00
Modified: 2020-11-04 06:38:00
Status: published

<img alt="WABEsense logo" src="{static}/images/wabesense_logo_color.png" width=300>


WABEsense is an innovation project funded by the [Environmental Technology 
Promotion of the Federal Office for the Environment (FOEN)](https://www.bafu.admin.ch/bafu/en/home/topics/education/innovation/umwelttechnologiefoerderung.html).

The project partners are:

- Software development: IET - Institute for Energy Technology, OST Eastern Switzerland University of Applied Sciences

- Hardware development: IMES - Institute for Microelectronics and Embedded Systems, OST Eastern Switzerland University of Applied Sciences

- Implementation: [Uli Lippuner AG](https://www.ulippuner.ch/de/home/), Sargans

## Abstract

The BAFU funded project [WABEsense](https://hsr-iet.gitlab.io/wabesense/blog/) will sensorize alpine water springs and monitor their water production.

During the project we will develop an open hardware solution that fits in the budget of municipality water utilities (in particular a data logger), and the IT infrastructure to share the data openly (to be located at https://opendata.swiss).
The water production data will allow municipalities to enhance their Generelle Wasserversorgungsplanung and their sustainability; and it can also be used by researchers to model the behavior of alpine water springs and water table.

**Why open hardware?** With the advent of open source and [open hardware](https://www.oshwa.org/sharing-best-practices/best-practices-der-open-source-hardware-1-0/), closed technological solutions are becoming less interesting.
Open technologies have clear advantages for many business: 1) they shift the focus to services and intangibles, 2) they build trust based on transparency and participation.
In 1) the product is now what you do with the technology and not the technology itself. You dominate the market when you do the best of the technology. Think of a tool in the hand of an expert craftsman: everybody knows how the tool works, everybody could build the tool, but only the expert craftsman can do magic with it.
In 2) clients can directly participate in the development of the technology, they become partners. They trust the technology because they can understand it, and influence it. Enhancement and development costs can be shared among many parties, the technology becomes a common good.

Municipal water utilities using WABEsense technology, are not trapped. They can repair, enhance, and hack with total freedom.
WABEsense was designed to be a truly open project implemented in an commercial environment and it will set a precedent on the impact of open projects.
       
## License
All the materials (including documentation, pictures, and drawings) on this website and in the repositories iss licensed under a [Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/).
Before using (copying, sharing or re-publishing, editintg, etc.) any of these materials make sure you have read the conditions on the license on this website https://creativecommons.org/licenses/by-sa/4.0/ 

Source code in the repositories is shared under a [GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.en.html) version 3 or later.
As before, make sure you understand the terms of the license before using (copying, editing, making derivative works) the source code.

## Members

<style>
table {
  border-collapse: collapse;
}
td {
  vertical-align: middle;
}
</style>

<table>
<tbody>
  <tr>
    <td style="vertical-align:middle;"><span style="font-weight:bold">IET</span></td>
    <td><img alt="JuanPi Carbajal" src="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/JP-Carbajal.jpg" width=100>
</td>
    <td style="vertical-align:middle;"><a href=https://sites.google.com/site/juanpicarbajal/>Juan Pablo Carbajal</a></td>
    <td><img alt="Alex Weber" src="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/Alex-Weber.png" width=100> 
</td>
    <td style="vertical-align:middle;">Alex Weber</td>
  </tr>
  <tr>
    <td style="vertical-align:middle;"><span style="font-weight:bold">IMES</span></td>
    <td><img alt="Erwin Brändle" src="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/Erwin-Braendle.jpg" width=100> 
</td>
    <td style="vertical-align:middle;">Erwin Brändle</td>
    <td><img alt="Adrian Tüscher" src="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/Adrian-Tuescher.jpg" width=100> 
</td>
    <td style="vertical-align:middle;">Adrian Tüscher</td>
  </tr>
  <tr>
    <td style="vertical-align:middle;"><span style="font-weight:bold">ULAG</span></td>
    <td><img alt="Jeannette Lippuner" src="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/jeannette_lippuner_sw.jpg" width=100> 
</td>
    <td style="vertical-align:middle;">Jeannette Lippuner</td>
    <td><img alt="Uli Lippuner" src="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/uli_lippuner_team_sw.jpg" width=100> 
</td>
    <td style="vertical-align:middle;">Uli Lippuner</td>
  </tr>
</tbody>
</table>

