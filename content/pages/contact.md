Title: Contact
Date: 2020-08-05 16:00:00
Modified: 2020-08-10 23:31:00
Slug: contact
Status: published

If you want to report typos, grammar and other errors in this blog, you can open an issue [here](https://gitlab.com/hsr-iet/wabesense/blog/-/issues).

Please use that site to request content modification or ask questions about the project.

