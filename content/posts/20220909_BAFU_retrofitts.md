Title: 15 WABEsense will be installed!
Date: 2022-09-09 12:00
Modified: 2022-09-09 12:00
Category: WABE, Data-logger
Tags: data-logger, project, bafu
Slug: bafu_retrofits
Authors: JuanPi Carbajal, Jeannette Lippuner
Summary: With 15 WABEsense systems installed on already existing infrastructure we reached a very important milestone of the project.
Status: published

One of the milestonesin the [WABEsense project](https://www.aramis.admin.ch/Grunddaten/?ProjectID=47484&Sprache=en-US) was the sensorization of 15 springs.
As of today we have reached this milestone.
At times we sought we would not make it in time.
The COVID pandemic caused complications and delays with the delivery of hardware (uC were not available).

Below is the table of WABEsense systems that will deliver data in the coming months:

| **Location**  | **Springs** | **Count** |
|:----------|:-------------|------:|
| Bonaduz | Paliu Fravi, Salums Friedrich, Leo, Fontauna Nera | 4 |
| Bregaglia | Acqua d’Balz 1 & 2 | 2 |
| Hergiswil | Müsli, Rossmoos | 2 |
| Oberriet | Ulrika | 1 |
| Schiers | Chalta Wasser, Grapp rechts | 2 |
| Zernez/Susch | Sarsura, Prada Bella suot, Prada Bella sura | 3 |
| Zug | Nidfuren | 1 |
| ** Total **| | 15 |


We are very excited!
