Title: WABE® - Historien 1
Date: 2020-11-04 14:39
Modified: 2020-11-04 14:39
Category: WABE
Tags: story, anecdotes
Slug: wabehistorien1
Author: Uli Lippuner
Summary: Brunnenstube für Trinkwasserversorgungen bestehen schon seit der Antike. Nur unwesentlich haben sie sich von Geometrie und Funktion verändert. Einige Jahre nach der Antike dh. im Jahr 1998 stellte Uli Lippuner an der 3. Fachtagung das System WABE ...
lang: de
Status: published

_Von Uli Lippuner_

Brunnenstube für Trinkwasserversorgungen bestehen schon seit der Antike. Nur unwesentlich haben sie sich von Geometrie und Funktion verändert. Einige Jahre nach der Antike dh. im Jahr 1998 stellte Uli Lippuner an der 3. Fachtagung das System WABE quasi als «Elchtest» einer exklusiven Erfindung anhand eines Pilotmodells vor (Abbildung 1+2). 

<figure style="width:100%;float:center">
<a href="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/WABE_historien_Ab1.png" target="_blank" title="Abbildung 1: Pilotmodell Vollfüllung">
<img style="max-width:50%" alt="Abbildung 1: Pilotmodell Vollfüllung" src="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/WABE_historien_Ab1.png"></img>
</a>

<a href="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/WABE_historien_Ab2.png" target="_blank" title="Abbildung 2: Pilotmodell mit Luftblasen im oberen Teil mit Entgasung">
<img style="max-width:50%" alt="Abbildung 2: Pilotmodell mit Luftblasen im oberen Teil mit Entgasung" src="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/WABE_historien_Ab2.png"></img></a>

<figcaption>Abbildung 1+2: Pilotmodell Vollfüllung und Pilotmodell mit Luftblasen im oberen Teil mit Entgasung.</figcaption>
</figure>

Der Beginn einer neuen Ära von Brunnenstuben die die Quellwassernutzung revolutionieren soll. Das System WABE (Wasser-Allround-Behälter) hat damit den Schritt von der Vision zur Realität erfolgreich geschafft. In nur vier Jahren intensiver und kontinuierlicher Entwicklungsarbeit hat die WABE-Technologie Fachleute überzeugen können, die sie anfänglich noch mit grosser Skepsis angesehen hatten. 

Die Idee zur WABE-Technik begann mit der Überzeugung von Uli Lippuner, dass unser wertvollstes Lebensmittel Trinkwasser besser geschützt werden sollte. So sind Gestern wie Heute noch zahlreiche Quellwässer zu wenig geschützt (Abbildung 3). 

<figure style="width:100%;float:center">
<a href="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/WABE_historien_Ab3.png" target="_blank" title="Abbildung 3: Alte Brunnenstube wie anno dazumal">
<img style="max-width:50%" alt="Abbildung 3: Alte Brunnenstube wie anno dazumal" src="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/WABE_historien_Ab3.png"></img></a>

<figcaption>Abbildung 3: Alte Brunnenstube wie anno dazumal.</figcaption>
</figure>

Noch allzu oft sprudelt das kostbare Nass wie vor 100 Jahren in altertümliche Brunnenstuben oder auch zahlreich in der Praxis gesehen, dass neu erstellte Brunnenstuben nach der altbewährten aber überholten Technik erstellt werden. Das Facility bzw. die inneren Einrichtungen wurden verbessert jedoch liess man die hydraulischen Komponenten und den Hygieneschutz für das Trinkwasser ausser Acht (Abbildung 4+5). 

<figure style="width:100%;float:center">
<a href="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/WABE_historien_Ab4.png" target="_blank" title="Abbildung 4: Überlastete konventionelle Brunnenstube">
<img style="max-width:49%" alt="Abbildung 4: Überlastete konventionelle Brunnenstube" src="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/WABE_historien_Ab4.png"></img></a>

<a href="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/WABE_historien_Ab5.png" target="_blank" title="Abbildung 5: Brunnenstube nach altbewährter Technik und Trübungsüberwachung">
<img style="max-width:49%" alt="Abbildung 5: Brunnenstube nach altbewährter Technik und Trübungsüberwachung" src="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/WABE_historien_Ab5.png"></img></a>

<figcaption>Abbildung 4+5: Überlastete konventionelle Brunnenstube und Brunnenstube nach altbewährter Technik und Trübungsüberwachung.</figcaption>
</figure>

Bei der Brunnenstube WABE standen folgende zentrale Überlegungen im Vordergrund. Geschlossenes System, keine offenen Wasserflächen, einfache Reinigungsarbeiten, kein Sicker- oder Oberflächenwasser das mit dem Quellwasser in Berührung kommt und nicht zuletzt die wirtschaftlichen, ökologischen wie betrieblichen Aspekte (Abbildung 6,7,8). 

<figure style="width:100%;float:center">
<a href="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/WABE_historien_Ab6.png" target="_blank" title="Abbildung 6: WABE Typ 3 in voller Funktion">
<img style="max-width:32%" alt="Abbildung 6: WABE Typ 3 in voller Funktion" src="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/WABE_historien_Ab6.png"></img></a>

<a href="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/WABE_historien_Ab7.png" target="_blank" title="Abbildung 7: WABE Typ 2 mit Volllast">
<img style="max-width:32%" alt="Abbildung 7: WABE Typ 2 mit Volllast" src="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/WABE_historien_Ab7.png"></img></a>

<a href="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/WABE_historien_Ab8.png" target="_blank" title="Abbildung 8: WABE-Funktionstest mit zwei Einläufen">
<img style="max-width:32%" alt="Abbildung 8: WABE-Funktionstest mit zwei Einläufen" src="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/WABE_historien_Ab8.png"></img></a>

<figcaption>Abbildung 6,7,8: WABE Typ 3 in voller Funktion. WABE Typ 2 mit Volllast. WABE-Funktionstest mit zwei Einläufen.</figcaption>

</figure>

Die WABE-Brunnenstube vereint in einem Zug all das, was in technischer und hygienischer Sicht der Lebensmittelgesetzgebung entspricht und zugleich auch ein hervorstechendes Multitalent das im Praxiseinsatz für die Wasserversorgungs-unternehmungen bedeutende Vorteile aufweist (Abbildung 9). 

<figure style="width:100%;float:center">
<a href="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/WABE_historien_Ab9.png" target="_blank" title="Abbildung 9: Brunnenstube mit WABE-Technik als geschlossenes System">
<img style="max-width:50%" alt="Abbildung 9: Brunnenstube mit WABE-Technik als geschlossenes System" src="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/WABE_historien_Ab9.png"></img></a>

<figcaption>Abbildung 9: Brunnenstube mit WABE-Technik als geschlossenes System.</figcaption>

</figure>

## Literatur

*  Uli Lippuner, System WABE «Elchtest» ,1998
*  Uli Lippuner, Publikation GWA, «Die geniale Lösung für das Quellgebiet», 1998
*  Uli Lippuner, Dokumentation, «Strömung und Betriebsverhalten der Brunnenstube System WABE», 1999
*  Uli Lippuner, System WABE «neues Verfahren in der Wasserversorgung», 2000
*  Roberto Pianta, Fachvortrag «WABE, ein Multitalent im Praxiseinsatz, 2002
*  Kurt Gloor, Publikation «200 innovative Brunnenstuben System WABE im Einsatz - ein Erfahrungsbericht», 2005
*  Uli Lippuner, Fachbuch «Quellwasser als natürliche Ressource», 2018
*  Uli Lippuner, Fachbuch «Quellwasser…», System WABE, S 137 – 152, 2018
