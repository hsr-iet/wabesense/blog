Title: Open Hardware Data-logger: alpha version
Date: 2020-12-17 11:00
Modified: 2021-03-13 02:27
Category: Data-logger
Tags: data, data-logger
Slug: datalogger0
Authors: Adrian Tüscher, JuanPi Carbajal
Summary: IMES has delivered the first version of the data-logger! Here some details about it.
Status: published

**UPDATE 14-06-21**: The logger is now updated to the [zero-series]({filename}20210614_DataLogger_zeroSeries.md)


IMES has delivered the first version of the data-logger!


It holds an ultra-low-power micro-controller of the [STM32L412xx](https://www.st.com/en/microcontrollers-microprocessors/stm32l412t8.html) family and is currently aimed at logging scenarios with low frequency sampling (of course, you can change that by hacking into the device).


The data-logger is Open Hardware you can reuse and modify it. Please **make sure** that you understand [the license](https://creativecommons.org/licenses/by-sa/4.0/) before you use design, images, or source code. The development files are available on the [hardware](https://gitlab.ost.ch/sciceg/lippunerag/wabesense/datalogger_hardware) and [firmware](https://gitlab.ost.ch/sciceg/lippunerag/wabesense/datalogger_firmware) repositories.

The data-logger was developed using FOSS software whenever possible, in particular we used [KiCad](https://kicad.org/) for the PCB.

 PCB top view    | PCB bottom view |
|-----------------|-----------------|
| <a href="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/datalogger_hardware/-/raw/main/doc/pictures/datalogger_alpha_PCB_Top.jpg" target="_blank" title="Data-logger front"><img alt="Data-logger front" src="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/datalogger_hardware/-/raw/master/doc/pictures/datalogger_alpha_PCB_Top.jpg"></img></a> | <a href="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/datalogger_hardware/-/raw/master/doc/pictures/datalogger_alpha_PCB_Bottom.jpg" target="_blank" title="Data-logger back"><img alt="Data-logger back" src="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/datalogger_hardware/-/raw/master/doc/pictures/datalogger_alpha_PCB_Bottom.jpg"></img></a>|

Below are the main features of the alpha version of the data-logger

* External channels
    * 4x Analog 0-5V
    * 4x Digital In/Out (opt. R-divider)
    * SPI or additional 4x Digital In/Out
    * I2C or additional 2x Digital In/Out

* Internal channels

    [BME280](https://www.bosch-sensortec.com/products/environmental-sensors/humidity-sensors-bme280/) sensor: pressure, temperature, humidity

* Data recording

    [Internal FLASH](https://www.adestotech.com/wp-content/uploads/AT25SF321B.pdf) (4 MB): up to 130000 data points (> 2 years @10 minute sampling)

* Data output exported to external SD-Card
    * Channel values (CSV file) 
    * Metadata and configuration (Plain text file)
    * History (Plain text file)

* Power source
    * 2xAA Batteries or accumulator: 2.5-12V DC
    * USB: 5V DC
    * External DC-Adapter (plug 2.1 x 5.5mm): 5.0-28V DC

* Configuration:
    * Device name string: max 16 characters
    * Analog measurement channel name strings
    * Battery: nominal cell voltage, number of cells
    * Sampling interval: 1-1092 minutes
    * System time: RTC with back-up battery (integrated in micro-controller)
    * Firmware update via SD-Card using .bin files

