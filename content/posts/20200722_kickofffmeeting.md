Title: Kickoff meeting
Date: 2020-07-22 12:00
Modified: 2020-08-10 23:31
Category: Meetings
Tags: planning, coordination
Slug: kickoff-meeting
Authors: JuanPi Carbajal
Summary: The project started with a very friendly kickoff meeting
Status: published

On July 22, 2020, the kickoff meeting took place.
The project WABEsense is alive!

<img alt="This one is just a literal element" src="{static}/images/wabesense_logo_color.png" width=300>

The participants were Juan Pablo Carbajal, Jeannette Lippuner,
Uli Lippuner, Erwin Brändle, and Adrian Tüscher.
We reviewed the project plan and adjusted the plant to match the actual dates.
The current project plan looks like this:

![project plant as of 2020-07-22](https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/2a4c99f2b35f6c51a2d1454e09c067deeb8ca415/doc/img/Gantt.svg)

Now we are working in the final version of the funding agreement.
Among other things it needs to consider that in September 1, 2020, the universities of applied sciences (FHS St.Gallen, HSR Rapperswil and NTB Buchs) will merge to become a single university of applied sciences: OST - Eastern Switzerland University of Applied Sciences.

Stay tuned ...
