Title: Feldinstallationen
Date: 2021-03-13 02:33
Modified: 2021-03-13 02:33
Category: WABE
Tags: story, anecdotes, data-logger
Slug: feldinstallationen
Author: Daniela Guardia-Lippuner
Summary: Im Januar und Februar 2021 durften wir die ersten Alpha Sites installieren. Mit einer Checkliste haben wir uns sauber vorbereitet, damit für die Installation vor Ort alles klappt. Schnell wurde klar, dass jede Situation und Ausgangslage vor Ort anders aussieht ...
lang: de
Status: published

_Von Daniela Guardia-Lippuner_

### Gleiche Vorbereitung - andere Ausgangslage
Im Januar und Februar 2021 durften wir die ersten Alpha Sites installieren. 
Mit einer Checkliste haben wir uns sauber vorbereitet, damit für die Installation vor Ort alles klappt.
Schnell wurde klar, dass jede Situation und Ausgangslage vor Ort anders aussieht.

### Wetter
Der Schnee liess lange auf sich warten, dafür gab es 2021 etwas mehr als in den letzten Jahren.
Die Brunnenstuben befinden sich nicht immer an befahrbaren Strassen, weshalb die Brunnenstube in Bonaduz freigelegt werden musste.

<figure style="float:center;font-weight:bold;margin-block:0;margin-inline:0">
<a href="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/Feldinstallationen_Ab1.png" target="_blank" title="Abbildung 1: Ausgraben von Schacht">
<img style="max-width:100%" alt="Abbildung 1: Ausgraben von Schacht" src="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/Feldinstallationen_Ab1.png"></img>
</a>

<figcaption>Abbildung 1: Ausgraben von Schacht.</figcaption>
</figure>

Die Schüttungsmengen stiegen wegen den starken Niederschlägen überdurchschnittlich an. 
Wir mussten die Installation während maximaler Durchfluss (Qmax ~ 2'000 L/min) durchführen.
Die erschwerten Bedingungen stellten sich für manuellen Referenzmessungen als eine Herausforderung dar.

Manuelle Schüttungsmessungen bei Q > 1'000 L/min sind schwierig und wie sich zeigt, sind die Abweichungen relativ gross.
Aufgrund der grossen Quellschüttungen überlief die eine oder andere WABE, was natürlich zu erhöhter Luftfeuchtigkeit in den Schächten führt. 
Die Öffnungen vom Datenlogger wurden provisorisch verschlossen.

<div style="display:flex;flex-flow:row wrap;justify-content:flex-start;margin-block:0;margin-inline:0">
<figure style="display:flex;flex-direction:column;font-weight:bold;width:250px;margin-block:0;margin-inline-start:1em">
<a href="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/Feldinstallationen_Ab2.png" target="_blank" title="Abbildung 2: Regen liess Schüttungsmenge auf Qmax steigt">
<img style="max-width:100%" alt="Abbildung 2: Regen liess Schüttungsmenge auf Qmax steigt" src="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/Feldinstallationen_Ab2.png"></img>
</a>

<figcaption>Abbildung 2: Regen liess Schüttungsmenge auf Qmax steigt.</figcaption>
</figure>

<figure style="display:flex;flex-direction:column;font-weight:bold;width:332px;margin-block:0;margin-inline:0">
<a href="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/Feldinstallationen_Ab3.png" target="_blank" title="Abbildung 3: Öffnungen verschliessen">
<img style="max-width:100%" alt="Abbildung 3: Öffnungen verschliessen" src="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/Feldinstallationen_Ab3.png"></img>
</a>

<figcaption>Abbildung 3: Öffnungen verschliessen.</figcaption>
</figure>

<iframe width="560" height="315" src="https://www.youtube.com/embed/tM-0Qo7edMI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="padding-top:1em"></iframe>

</div>


### Montage
Es stellt sich jeweils die Frage, wo der Datenlogger befestigt wird.
Während den Arbeiten stellten wir fest, dass das Kabel vom Sensor zum Datenlogger in gewissen Situationen zu kurz ist für die gewünschte Montagestelle.
An der alternativen Position war der Platz dann etwas knapp, was die Installation und Inbetriebnahme wiederum erschwerte.

<figure style="width:100%;font-weight:bold;margin-block:0;margin-inline:0">
<a href="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/Feldinstallationen_Ab4.png" target="_blank" title="Abbildung 4: Montagestelle Datenlogger">
<img style="max-width:47%" alt="Abbildung 4: Montagestelle Datenlogger" src="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/Feldinstallationen_Ab4.png"></img>
</a>

<a href="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/Feldinstallationen_Ab5.png" target="_blank" title="Abbildung 5: Montagestelle Sensor">
<img style="max-width:51.5%" alt="Abbildung 5: Montagestelle Sensor" src="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/Feldinstallationen_Ab5.png"></img>
</a>

<figcaption>Abbildung 4+5: Montage von Datenlogger und Sensor.</figcaption>
</figure>
</div>

Die Zugänglichkeit zum Datenlogger ist für den Brunnenmeister sicherzustellen, damit er
jederzeit manuelle Referenzmessungen mit dem Datenlogger ausführen kann.

Die WABE hat keine Flanschverbindung und die WABE kann zum Zeitpunkt der max. Schüttungsmenge überlaufen, weshalb der Datenlogger nicht an der WABE selber befestigt werden kann. 
Die Montage am inneren Schachtring war wegen der Feuchtigkeit ebenfalls nicht ideal.
Somit wurde der Datenlogger an dem Quelleinlauf installiert.

<figure style="width:100%;font-weight:bold;margin-block:0;margin-inline:0">
<a href="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/Feldinstallationen_Ab6.png" target="_blank" title="Abbildung 6: WABE Einlauf ohne Flansch → Alternative Montage">
<img style="max-width:100%" alt="Abbildung 6: WABE Einlauf ohne Flansch → Alternative Montage" src="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/Feldinstallationen_Ab6.png"></img>
</a>

<figcaption>Abbildung 6: WABE Einlauf ohne Flansch → Alternative Montage.</figcaption>

<a href="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/Feldinstallationen_Ab7.png" target="_blank" title="Abbildung 7: Enge Schachtverhältnisse erschwerten die Arbeiten im Schacht">
<img style="max-width:100%" alt="Abbildung 7: Enge Schachtverhältnisse erschwerten die Arbeiten im Schacht" src="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/Feldinstallationen_Ab7.png"></img>
</a>

<figcaption>Abbildung 7: Enge Schachtverhältnisse erschwerten die Arbeiten im Schacht.</figcaption>
</figure>


<figure style="display:flex;flex-direction:column;font-weight:bold;width:20%;margin-block:0;margin-inline:0;float:right">
<a href="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/Feldinstallationen_Ab8.png" target="_blank" title="Abbildung 8: T-Stück für Sensor und Probehahn">
<img style="max-width:100%;" alt="Abbildung 8: T-Stück für Sensor und Probehahn" src="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/Feldinstallationen_Ab8.png"></img>
</a>

<figcaption>Abbildung 8: T-Stück für Sensor und Probehahn.</figcaption>
</figure>

*T-Stücke sind essentiell*.
Der Brunnenmeister hat oftmals nur die Möglichkeit in seiner Brunnenstube die Quellschüttung zu messen.
Damit der Sensor nicht jedes Mal herausgeschraubt wird, wird ein T-Abgang montiert.
Folglich ist die Datenerfassung und Probenahme gleichzeitig möglich.

### Technik - IT
Die Sicherheitssysteme sind bei jeder Wasserversorgung anders aufgebaut.
In diesem Fall konnte der Stecker nicht einfach eingesteckt werden, sondern es musste vor Ort nach möglichen Lösungen gesucht werden, damit die Daten übermittelt wurden.

<figure style="width:70%;font-weight:bold;margin-block:0;margin-inline:0">
<a href="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/Feldinstallationen_Ab9.png" target="_blank" title="Abbildung 9: Lösungssuche vor Ort">
<img style="max-width:100%" alt="Abbildung 9: Lösungssuche vor Ort" src="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/Feldinstallationen_Ab9.png"></img>
</a>

<figcaption>Abbildung 9: Lösungssuche vor Ort.</figcaption>
</figure>
<br style="clear:both" />


Die Datenlogger sind jetzt an ihrer Stelle und funktionieren. Wir sammeln Daten von den Alpha Sites und polieren alle rauen Spots.
Bald werden wir ein Dashboard zur Visualisierung der Daten veröffentlichen.

Bleiben Sie dran!
