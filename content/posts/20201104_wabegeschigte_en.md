Title: WABE® - History 1
Date: 2020-11-04 14:39
Modified: 2020-11-04 14:39
Category: WABE
Tags: story, anecdotes
Slug: wabehistorien1
Authors: Uli Lippuner, JuanPi Carbajal
Summary: Springboxes for drinking water supplies have existed since ancient times. Their geometry and function have changed only insignificantly. A few years after antiquity, i.e. in 1998, Uli Lippuner presented the WABE system at the 3rd water symposium ...
lang: en
Status: published

Springboxes for drinking water supplies have existed since ancient times. Their geometry and function have changed only insignificantly. A few years after antiquity, i.e. in 1998, Uli Lippuner presented the WABE system at the 3rd water symposium as a "moose test" of an exclusive invention on the basis of a pilot model (Figures 1+2).

<figure style="width:100%;float:center">
<a href="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/WABE_historien_Ab1.png" target="_blank" title="Figure 1: Pilotmodell Vollfüllung">
<img style="max-width:50%" alt="Figure 1: Pilotmodell Vollfüllung" src="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/WABE_historien_Ab1.png"></img>
</a>

<a href="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/WABE_historien_Ab2.png" target="_blank" title="Figure 2: Pilotmodell mit Luftblasen im oberen Teil mit Entgasung">
<img style="max-width:50%" alt="Figure 2: Pilotmodell mit Luftblasen im oberen Teil mit Entgasung" src="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/WABE_historien_Ab2.png"></img></a>

<figcaption>Figure 1+2: Pilot model filling and degassing of air bubbles.</figcaption>
</figure>

That marked the beginning of a new era of springboxes that would revolutionize the exploitation of spring water. The WABE system (from German, _Wasser-Allround-Behälter_) had thus successfully made the step from vision to reality. In only four years of intensive and continuous development work, the WABE technology was able to convince experts, who had initially viewed it with great skepticism. 

The idea for the WABE technology started with the conviction of Uli Lippuner that our most valuable foodstuff, drinking water, should be better protected. However, yesterday as well as today, numerous springs are still insufficiently protected (Figure 3). 

<figure style="width:100%;float:center">
<a href="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/WABE_historien_Ab3.png" target="_blank" title="Figure 3: Alte Brunnenstube wie anno dazumal">
<img style="max-width:50%" alt="Figure 3: Alte Brunnenstube wie anno dazumal" src="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/WABE_historien_Ab3.png"></img></a>

<figcaption>Figure 3: Old springbox like in the ancient times.</figcaption>
</figure>

Still, all too often, the precious water gushes into old springboxes as it did 100 years ago. Further, it is often seen in practice, that newly built boxes are constructed according to old and outdated techniques. The facilities and internal equipment are improved, but the hydraulic components and the hygienic protection of the drinking water is disregarded (Figure 4+5). 

<figure style="width:100%;float:center">
<a href="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/WABE_historien_Ab4.png" target="_blank" title="Figure 4: Überlastete konventionelle Brunnenstube">
<img style="max-width:49%" alt="Figure 4: Überlastete konventionelle Brunnenstube" src="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/WABE_historien_Ab4.png"></img></a>

<a href="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/WABE_historien_Ab5.png" target="_blank" title="Figure 5: Brunnenstube nach altbewährter Technik und Trübungsüberwachung">
<img style="max-width:49%" alt="Figure 5: Brunnenstube nach altbewährter Technik und Trübungsüberwachung" src="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/WABE_historien_Ab5.png"></img></a>

<figcaption>Figure 4+5: Overloaded conventional springbox and well room according to longstanding technology and turbidity monitoring.</figcaption>
</figure>

For the springbox WABE the following central considerations were in the foreground: a closed system, no exposed water surfaces, simple cleaning work, no seepage or surface water that comes into contact with the spring water, and last but not least, the economic, ecological and operational aspects (Figure 6,7,8)

<figure style="width:100%;float:center">
<a href="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/WABE_historien_Ab6.png" target="_blank" title="Figure 6: WABE Typ 3 in voller Funktion">
<img style="max-width:32%" alt="Figure 6: WABE Typ 3 in voller Funktion" src="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/WABE_historien_Ab6.png"></img></a>

<a href="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/WABE_historien_Ab7.png" target="_blank" title="Figure 7: WABE Typ 2 mit Volllast">
<img style="max-width:32%" alt="Figure 7: WABE Typ 2 mit Volllast" src="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/WABE_historien_Ab7.png"></img></a>

<a href="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/WABE_historien_Ab8.png" target="_blank" title="Figure 8: WABE-Funktionstest mit zwei Einläufen">
<img style="max-width:32%" alt="Figure 8: WABE-Funktionstest mit zwei Einläufen" src="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/WABE_historien_Ab8.png"></img></a>

<figcaption>Figure 6,7,8: WABE type 3 in full function. WABE type 2 with full load. WABE function test with two inlets.</figcaption>

</figure>

The WABE combines in a single unit all the technical and hygienic features that comply with swiss food legislation and at the same time is an outstanding all-rounder that offers significant advantages in practical use for water supply companies (Figure 9). 

<figure style="width:100%;float:center">
<a href="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/WABE_historien_Ab9.png" target="_blank" title="Figure 9: Brunnenstube mit WABE-Technik als geschlossenes System">
<img style="max-width:50%" alt="Figure 9: Brunnenstube mit WABE-Technik als geschlossenes System" src="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/WABE_historien_Ab9.png"></img></a>

<figcaption>Figure 9: WABE technology as a closed system.</figcaption>

</figure>

## Further reading

*  Uli Lippuner, System WABE «Elchtest» ,1998
*  Uli Lippuner, Publikation GWA, «Die geniale Lösung für das Quellgebiet», 1998
*  Uli Lippuner, Dokumentation, «Strömung und Betriebsverhalten der Brunnenstube System WABE», 1999
*  Uli Lippuner, System WABE «neues Verfahren in der Wasserversorgung», 2000
*  Roberto Pianta, Fachvortrag «WABE, ein Multitalent im Praxiseinsatz, 2002
*  Kurt Gloor, Publikation «200 innovative Brunnenstuben System WABE im Einsatz - ein Erfahrungsbericht», 2005
*  Uli Lippuner, Fachbuch «Quellwasser als natürliche Ressource», 2018
*  Uli Lippuner, Fachbuch «Quellwasser…», System WABE, S 137 – 152, 2018
