Title: Von der Handmessung zur automatischen Zukunft
Date: 2021-03-16 17:46
Modified: 2021-03-24 10:50
Category: WABE
Tags: story, anecdotes
Slug: handzurautomatisch
Authors: Uli Lippuner, Jeannette Lippuner
Summary: Quellwassermessungen zeigen immer wieder das gleiche Verhaltensschema in Bezug auf die Messeinrichtung, Auswertung und Interpretation der Messungen, indem Monats- und Jahresganglinien mit aufwendigen und zugleich rudimentären Methoden realisiert werden. Diese gemessenen werte ermöglichen einen kleinen Einblick in das Quellenregime und geben unter Umständen für die Planung ein falsches Bild ab ...
lang: de
Status: published

_Von Uli & Jeannette Lippuner_

Seit über 45 Jahren planen und beraten wir (ULAG) Wasserversorgungsunternehmen.
Dazu gehört auch die Spezialität „Quellwassernutzung“ welches nach wie vor ein zentrales Standbein innerhalb der Wasserversorgung bezüglich dem Wasserdargebot darstellt.
In diesem Zusammenhang sind die natürlich zu Tage tretenden Quellwasser von Interesse, da sie doch in der Schweiz einen Beitrag zum Wasserhaushalt von ca. 40% beitragen.
Die Beobachtungen zeigen immer wieder das gleiche Verhaltensschema in Bezug auf die Messeinrichtung, Auswertung und Interpretation der Messungen, indem Monats- und Jahresganglinien mit aufwendigen und zugleich rudimentären Methoden realisiert werden.
In den meisten Fällen werden periodische Handmessungen in einem monatlichen Turnus durchgeführt (Abbildung 1).
Diese gemessenen Mittelwerte ermöglichen einen kleinen Einblick in das Quellenregime und geben unter Umständen für die Planung ein falsches Bild ab. 

<figure style="float:center;font-weight:bold;margin-block:0;margin-inline:0">
<a href="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/HandzurAutomatisch_Ab1.png" target="_blank" title="Abbildung 1: Diagramm mit Referenzmesspunkten über 7 Jahre">
<img style="max-width:100%" alt="Abbildung 1: Diagramm mit Referenzmesspunkten über 7 Jahre" src="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/HandzurAutomatisch_Ab1.png"></img>
</a>

<figcaption>Abbildung 1: Diagramm mit Referenzmesspunkten über 7 Jahr.</figcaption>
</figure>

Es werden in vielen Fällen nicht die einzelnen Quell- oder Grundwasser ermittelt, sondern die Summe von ganzen Gebieten mit diversen Quellen d.h., dass die Ergiebigkeit nie vollständig transparent dargestellt werden kann.
Auch werden die Quellschüttungen in alpinen und voralpinen Gegenden im Winterzyklus (Monat November - April) kaum gemessen, da sie sehr schwer oder überhaupt nicht zugänglich sind (Abbildung 2).
In dieser Abbildung ist gut erkennbar, dass die wichtigste Periode im Jahreschart fehlt.

<figure style="float:center;font-weight:bold;margin-block:0;margin-inline:0">
<a href="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/HandzurAutomatisch_Ab2.png" target="_blank" title="Abbildung 2: Jahresganglinie mit dem Fehlen der Wintermonate">
<img style="max-width:100%" alt="Abbildung 2: Jahresganglinie mit dem Fehlen der Wintermonate" src="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/HandzurAutomatisch_Ab2.png"></img>
</a>

<figcaption>Abbildung 2: Jahresganglinie mit dem Fehlen der Wintermonate.</figcaption>
</figure>

Aus diesen Überlegungen heraus ist es sinnvoll, dass eine erleichterte automatische Messmethode das Analysieren von Schüttungs- und Temperaturschwankungen in Bezug auf die Verweildauer im Untergrund und dem Verhältnis von kleinster und grösster Schüttung darlegt.
Dazu eignet sich die WABEsense-Technologie hervorragend und leistet gleichzeitig einen wichtigen Beitrag zur Digitalisierung in der Wasserbranche.

Die Abbildungen 3+4 zeigen exemplarisch, wie die Digitalisierung in den Wasserversorgungen noch in weiter Ferne liegt.
Eine typische reale Situation, wie sie in vielen 100-ten von Wasserversorgungsunternehmen anzutreffen ist.

<div style="display:flex;flex-flow:row wrap;justify-content:flex-start;margin-block:0;margin-inline:0">
<figure style="display:flex;flex-direction:column;font-weight:bold;width:630px;margin-block:0;margin-inline-start:1em">
<a href="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/HandzurAutomatisch_Ab3.png" target="_blank" title="Abbildung 3: Diverse Daten in einer Schachtel gelagert">
<img style="max-width:100%" alt="Abbildung 3: Diverse Daten in einer Schachtel gelagert" src="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/HandzurAutomatisch_Ab3.png"></img>
</a>

<figcaption>Abbildung 3: Diverse Daten in einer Schachtel gelagert.</figcaption>
</figure>

<figure style="display:flex;flex-direction:column;font-weight:bold;width:350px;margin-block:0;margin-inline:0">
<a href="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/HandzurAutomatisch_Ab4.png" target="_blank" title="Abbildung 4: Diverse Qualitäts-Parameter">
<img style="max-width:100%" alt="Abbildung 4: Diverse Qualitäts-Parameter" src="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/HandzurAutomatisch_Ab4.png"></img>
</a>

<figcaption>Abbildung 4: Diverse Qualitäts-Parameter.</figcaption>
</figure>

</div>

## Literatur

* Uli Lippuner, Fachbuch "[Quellwasser als natürliche Ressource](https://www.lipartner.ch/de/publikationen/fachbuch-quellwasser)", 2018

