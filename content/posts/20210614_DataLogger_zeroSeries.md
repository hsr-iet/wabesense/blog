Title: Open Hardware Data-logger: zero series version
Date: 2021-06-14 13:00
Modified: 2021-06-14 13:54
Category: Data-logger
Tags: data, data-logger
Slug: datalogger_zeroseries
Authors: Adrian Tüscher, JuanPi Carbajal
Summary: IMES has delivered the zero series version of the data-logger! Here some details about it.
Status: published

IMES has delivered the zero series version of the data-logger! 
This is an update after the experience we had with the [alpha version]({filename}20201217_DataLogger0.md)

This is a summary of the most important changes that were done in this update:

- Changing the micro-controller from STM32L412 to STM32L475 to support the USB-C protocol.

- Serial interface, data transfer, and upgrade over USB-C port

- Discarding the integrated battery support

- Adding more sensor connectors to use the 4 analog channels available

- Non-soldering connectors for buttons and LED

- Connector used by ST for the programmer/debugger device

The new version holds an ultra-low-power micro-controller with USB OTG interface of the [STM32L475xx](https://www.st.com/en/microcontrollers-microprocessors/stm32l475re.html) family and is currently aimed at logging scenarios with low frequency sampling (of course, you can change that by modifying the firmware).

The data-logger is Open Hardware you can reuse and modify it. Please **make sure** that you understand [the license](https://creativecommons.org/licenses/by-sa/4.0/) before you use design, images, or source code. The development files are available on the [hardware](https://gitlab.ost.ch/sciceg/lippunerag/wabesense/datalogger_hardware) and [firmware](https://gitlab.ost.ch/sciceg/lippunerag/wabesense/datalogger_firmware) repositories.

The data-logger was developed using FOSS software whenever possible, in particular we used [KiCad](https://kicad.org/) for the PCB.

 PCB top view    | PCB bottom view |
|-----------------|-----------------|
| <a href="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/datalogger_zeroSeries_PCB_Top_annotated.png" target="_blank" title="Data-logger front"><img alt="Data-logger front" src="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/datalogger_zeroSeries_PCB_Top_annotated.png"></img></a> | <a href="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/datalogger_zeroSeries_PCB_Bottom_annotated.png" target="_blank" title="Data-logger back"><img alt="Data-logger back" src="https://gitlab.ost.ch/sciceg/lippunerag/wabesense/wabesense/-/raw/main/doc/img/datalogger_zeroSeries_PCB_Bottom_annotated.png"></img></a>|

Below are the main features of the zero series version of the data-logger

* External channels
    * 4x Analog 0-5 V
    * 2x Analog 0-3.3 V (VDDA)
    * 7x Digital In/Out
    * SPI interface or additional 4x Digital In/Out
    * I2C interface or additional 2x Digital In/Out
    * V_Supply (USB or external DC-Adapter)
    * 3.3 V supply
    * 3.3 V supply switched by micro-controller
    * 17 V supply switched by micro-controller
    * 3.3 V VDDA supply 

* Internal channels

    * [BME280](https://www.bosch-sensortec.com/products/environmental-sensors/humidity-sensors-bme280/) sensor: pressure, temperature, humidity

* Data recording

    * [Internal FLASH](https://www.adestotech.com/wp-content/uploads/AT25SF321B.pdf) (4 MB): up to 130000 data points (> 2 years @10 minute sampling interval)

* Data output exported to external memory stick (USB-C connector)
    * Channel values (CSV file) 
    * Metadata and configuration (Plain text file)
    * Logger activity log (Plain text file)

* Input power source
    * USB-C connector: 5 V DC
    * External DC-Adapter (plug 2.1 x 5.5 mm): 5.0-28 V DC or 

* Configuration over USB-C connector (virtual COM-Port):
    * Device name string: max. 92 characters
    * Analog channel name string: max. 64 characters
    * Battery: nominal cell voltage, number of cells (used for system check)
    * Sampling interval: 1-1092 minutes
    * System time: RTC with back-up battery CR2032 (RTC unit is integrated in micro-controller)

* Firmware update:
    * Via memory stick using .bin (binary, compiled firmware) files (USB-C connector)

## Things we changed

**Serial interface, data transfer, and upgrade over USB-C port**: the alpha series logger allowed the user to configure the logger and run some simple commands using a serial port over USB (B, for the alpha series). The SD-card was used to download the logged data and to upload new firmware versions (starting from firmware 1.6.0).
On the zero-series logger this is now all done over the USB-C port interface.
This allows for configuration, maintenance, and use of the logger all over a 
single interface.
The flashing of a new firmware is still done over an specialized connector on the logger, which you can see just above the WABEsense logo in the PCB bottom view.
The main driver to move to the USB interface, however, is somehow unexpected. 
In the several months that we have been observing the alpha series logger, we noted that our measures to avoid exposing the logger to high humidity for prolonged periods of time time was not working satisfactorily.
The need for a tighter logger housing was clear.
We did not find good housings with tight SD-card slots (e.g. IP64-66), and we did easily found them for USB-C ports.
Hence we switched the interface to USB-C, which brought the advantage mentioned above, but also forced us to update the micro-controller.
This is because the STM32L412 family, used for the alpha series, does not support USB-C.

**Integrated battery support**: the logger already provides an external source input via the DC plug.
The integrated batteries increased the complexity of the handling of the logger, hence we decided to directly use the DC plug to connect the external batteries. 
This brought the additional advantage of simplifying the electronics of the logger.

**Adding more sensor connectors**: the alpha version allowed to connect 2 analog sensors.
This kept the first version simple.
Now that the functionality is tested and works well, we opened up the connections to the additional
2 analog channels.

**Non-soldering connectors for buttons and LED**: to ease the exchange of a failing button or LEDs, these parts are integrated in the logger via connectors, no need for (de)soldering.
 
**ST connector for the programmer/debugger device**: to flash and debug the logger one needs a
programmer device (currently we use ST [tools](https://www.st.com/en/development-tools/st-link-v2.html) and [software](https://www.st.com/en/development-tools/stm32cubeprog.html), but we will liberate the toolchain form these dependencies). 
The alpha version had a smaller connector which required an adapter to connect the ST programmer.
To simplify this, we put an ST compatible connector on the logger.

## Things we kept

**Internal flash memory**: the conceptual design of the logger did not have any internal
storage.
Adrian Tüscher was quick to upgrade that design to include the 4 MB FLASH into the alpha version.
This was a great idea so we kept this addition to the final version of the logger.
This allows for simple management of the logged data, and for low sampling rates is probably all you are going to use for storage.

**Working SD-card module**: although the data transfer now happens over the USB-C port we kept a functional SD-card module. 
The SD-card acts like an internal hard-disk.
This is because having the possibility to add large non-volatile storage has many advantages.
For example, if somebody set the logger to log every minute, the current minimum sampling period, the internal storage would last for little more than 90 days. 
In such scenario data could be dumped into an SD-card and extend the logging records to many years.
Another scenario is larger amounts of metadata.
Currently, information about the logger (logs, configuration, etc.) is stored in the internal FLASH, this takes up space and could be stored in an SD-card. 
This also allows to store more metadata like sensor calibrations, maintenance notes, location information, etc.

