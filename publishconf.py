#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

# This file is only used if you use `make publish` or
# explicitly specify it as your config file.

import os
import sys
sys.path.append(os.curdir)
from pelicanconf import *

# If your site is available via HTTPS, make sure SITEURL begins with https://
SITEURL = 'https://hsr-iet.gitlab.io/wabesense/blog'
SITELOGO = SITEURL + '/images/wabesense_logo.svg'
RELATIVE_URLS = False

MENUITEMS = (('Archives', SITEURL + '/archives.html'),
             ('Categories', SITEURL + '/categories.html'),
             ('Tags', SITEURL + '/tags.html'),)

# Update SOCIAL  links that need SITEURL
SOCIAL = tuple((s[0], SITEURL+s[1]) if s[0] in ('rss', 'atom') else s
          for s in SOCIAL)

FEED_DOMAIN = SITEURL
FEED_ALL_ATOM = 'feeds/all.atom.xml'
FEED_ALL_RSS = 'feeds/all.rss.xml'
CATEGORY_FEED_ATOM = 'feeds/{slug}.atom.xml'
CATEGORY_FEED_RSS = 'feeds/{slug}.rss.xml'

DELETE_OUTPUT_DIRECTORY = True

# Following items are often useful when publishing

DISQUS_SITENAME = "wabesenseblog"
#GOOGLE_ANALYTICS = ""

USE_LESS = False
